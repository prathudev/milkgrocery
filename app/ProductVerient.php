<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductVerient extends Model
{
    protected $table='product_varient';
    protected $primaryKey = 'product_id';
    protected $guarded =[];
}
