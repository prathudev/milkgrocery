<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use DB;
use App\Traits\SendMail;
use App\Traits\SendSms;
use Carbon\Carbon;
class DailyOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'daily:order';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Schedule Daily Order for subscription';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $created_at = Carbon::now();
      
        $orders = DB::table('orders')
        ->where([
            ['type', '=', 'subscription'],
            ['sub_status', '=', 'active'],
        ])
        ->get(); 
        foreach($orders as $order){
            Log::info($order->cart_id);
            $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $val = "";
            for ($i = 0; $i < 4; $i++){
                $val .= $chars[mt_rand(0, strlen($chars)-1)];
            }
            
            $chars2 = "0123456789";
                    $val2 = "";
                    for ($i = 0; $i < 2; $i++){
                        $val2 .= $chars2[mt_rand(0, strlen($chars2)-1)];
                    }        
            $cr  = substr(md5(microtime()),rand(0,26),2);
            $cart_id = $val.$val2.$cr;

            $store_orders =  DB::table('store_orders')->where('order_cart_id',$order->cart_id)->get();
            foreach($store_orders as $store_order){
                // Log::info($store_order->store_order_id);

                $insert = DB::table('store_orders')
                ->insertGetId([
                        'varient_id'=>$store_order->varient_id,
                        'qty'=>$store_order->qty,
                        'product_name'=>$store_order->product_name,
                        'varient_image'=>$store_order->varient_image,
                        'quantity'=>$store_order->quantity,
                        'unit'=>$store_order->unit,
                        'total_mrp'=>$store_order->total_mrp,
                        'order_cart_id'=>$cart_id,
                        'order_date'=> $created_at,
                        'price'=>$store_order->price]);

            }
            $prii=($order->total_price)/30;
            $oo = DB::table('orders')
            ->insertGetId(['cart_id'=>$cart_id,
            'total_price'=>$prii,
            'price_without_delivery'=>$prii,
            'total_products_mrp'=>$order->total_products_mrp,
            'delivery_charge'=>$order->delivery_charge,
            'user_id'=>$order->user_id,
            'store_id'=>$order->store_id,
            'rem_price'=>'0',
            'order_date'=> $created_at,
            'delivery_date'=> $created_at,
            'time_slot'=>$order->time_slot,
            'type'=>'schedule',
            'paid_by_wallet'=>$prii,
            'payment_method'=>'wallet',
            'address_id'=>$order->address_id]); 

            $ph2 = DB::table('users')
            ->select('user_phone','wallet')
            ->where('user_id',$order->user_id)
            ->first();
            $rem_wallet2=$ph2->wallet-$prii;
            $walupdate2 = DB::table('users')
            ->where('user_id',$order->user_id)
            ->update(['wallet'=>$rem_wallet2]);
              
            Log::info($cart_id);
        }

    }
           
    
    
  
           
}
